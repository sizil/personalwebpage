define([
  'jquery',
  'underscore',
  'backbone',
  'handlebars',
  'jcookie',
  '../app',
  'text!templates/tags.html',
  'text!templates/archive.html',
  'text!templates/loader.html',
  'text!templates/404.html',
  'views/archive'
], function ($, _, Backbone, Handlebars, jcookie, app, tagsHTML, archiveHTML, loaderHTML, fofHTML, ArchivePage) {
    var TagsPage = Backbone.View.extend({
        el: $('#indexcontent'),
        render: function (tag) {
           
                archivePage = new ArchivePage();
                archivePage.render(null, null, null, tag);

        }
    });
    return TagsPage;
});