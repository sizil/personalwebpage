define([
  'jquery',
  'underscore',
  'backbone',
  'handlebars',
  'jcookie',
  '../app',
  'text!templates/home.html'
], function ($, _, Backbone, Handlebars, jcookie, app, homeHTML) {
    var HomePage = Backbone.View.extend({
        initialize: function (options) {
            this.options = options;
            _.bindAll(this, 'beforeRender', 'render', 'afterRender');
            var _this = this;
            this.render = _.wrap(this.render, function (render) {
                _this.beforeRender();
                render(_this.options['route']);
                _this.afterRender(_this.options['route']);
                return _this;
            });
        },

        beforeRender: function () {
            
        },

        el: $('#indexcontent'),
        
        template: _.template(homeHTML, {}),
        
        render: function (a) {
            
            this.$el.html(this.template);

        },

        afterRender: function (a) {
            
            if(window.location.hash === '#home') ScrollTo('.main-content');
        }
    });

    
    
    return HomePage;
});