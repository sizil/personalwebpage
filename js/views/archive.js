    define([
      'jquery',
      'underscore',
      'backbone',
      'handlebars',
      'jcookie',
      '../app',
      'text!templates/archive.html',
      'text!templates/404.html',
      'text!templates/loader.html',

    ], function ($, _, Backbone, Handlebars, jcookie, app, archiveHTML, fofHTML, loaderHTML) {


        var PostID = 0;
        var Tags = {};

        var TagCloud = Backbone.View.extend({
            initialize: function (options) {
                this.el = $('#sidenavitem-tagcloud-content-box');
                this.target_el = $('#archive-side-nav .tag-cloud .archive-side-nav-item-text');
            },
            render: function () {
                var _this = this;

                $.ajax({
                    type: "GET",
                    url: "includes/server_api.php/tagsinpost as TP, Tags as T/TP.tid=T.id group by T.tagname/distinct T.tagname, count(*) as num",
                    data: '',
                    cache: false,
                    success: function (html) {
                        var obj = JSON.parse(html);
                        var template = _.template(_this.el.html(), obj);
                        _this.target_el.html(template);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });

            }

        });

        var SideArchiveNavigation = Backbone.View.extend({
            initialize: function (options) {
                this.el = null;
                this.target_el = $('#archive-side-nav #category-3 .archive-side-nav-item');
            },
            render: function () {
                var _this = this;
                var struct2 = {};
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var psuedoResults = '';
                $.ajax({
                    type: "GET",
                    url: "includes/server_api.php/post/id > 0 order by date asc/distinct id, heading, date",
                    data: '',
                    cache: false,
                    success: function (html) {
                        var obj = JSON.parse(html);
                        obj.forEach(function (row) {
                            var date = new Date(row.date); // will extract date parts form Date object
                            var yyyy = date.getFullYear();
                            var mm = months[date.getMonth()]; // months come zero-indexed (from 0-11)
                            if (mm < 10) mm = '0' + mm; // left pad if needed
                            struct2[yyyy] = struct2[yyyy] || {}; // init if needed
                            struct2[yyyy][mm] = struct2[yyyy][mm] || []; // init if needed
                            delete row.date; // drop date if desired
                            struct2[yyyy][mm].push(row); // add new record
                        });
                        psuedoResults = '<ul style="margin-left:1em;">';
                        for (var id in struct2) {
                            psuedoResults += '<li><a href="#archive/' + id + '">' + id + ' (' + Object.keys(struct2[id]).length + ')</a></li><ul>';
                            for (var _id in struct2[id]) {
                                psuedoResults += '<li style="margin-left:1em;"><a href="#archive/' + id + '/' + (months.indexOf(_id) + 1) + '">' + _id + ' (' + Object.keys(struct2[id][_id]).length + ')</a></li>';
                            }
                            psuedoResults += '</ul>';
                        }
                        psuedoResults += '</ul>';
                        _this.target_el.html(psuedoResults);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });

            }

        });


        var CommentsOnPost = Backbone.View.extend({
            initialize: function (options) {
                this.el = $("#comments-content-box");
                this.target_el = $("#comments");
            },
            render: function () {
                var _this = this;
                var dataString = encodeURIComponent('pid=' + PostID + ' order by date desc');
                $("#comments").html(loaderHTML);
                $.ajax({
                    type: "GET",
                    url: "includes/server_api.php/comments/" + dataString,
                    data: '',
                    cache: false,
                    success: function (html) {
                        var obj = JSON.parse(html);
                        var template = _.template(_this.el.html(), obj);
                        _this.target_el.html(template);


                        /*_.each(obj, function(val, index) {
                            index += 1;
                            $("#comments").prepend('<div style="padding: 20px;" id="archivehead"> \
                                                        <div style="float: right" class="deletecomm"> </div> \
                                                        <p class="msg" style="margin-bottom: 5px;font-style: italic;">(#'+index+'/'+obj.length+') Anonymous</p> \
                                                        <p style="margin-bottom: 5px;"> '+val.comment+'</p> \
                                                        <h5 class="msg" style="font-style: italic;">'+val.date+'</h5> \
                                                    </div>');
                        });*/
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });

            }

        });

        var TagsInPost = Backbone.View.extend({
            initialize: function (options) {
                this.el = null;
                this.target_el = $("#archive-side-nav #item2 .archive-side-nav-item-text");
            },
            render: function () {
                var _this = this;
                $.ajax({
                    type: "GET",
                    url: "includes/server_api.php/tags as T natural join tagsinpost as TP/T.id=TP.tid and TP.pid=" + PostID + "/distinct T.tagname, T.id",
                    data: '',
                    cache: false,
                    success: function (html) {
                        var obj = JSON.parse(html);
                        /*                           var template = _.template($("#tags-content-box").html(), obj);
                                                   $(".tagholder").html(template);*/
                        Tags = '';
                        _.each(obj, function (val, index) {
                            Tags += '<a  href="#tag/' + val.tagname + '" class="opentags " id="' + val.id + '">' + val.tagname + '</a>';
                            if (obj.length > index + 1) Tags += ', ';
                        });
                        _this.target_el.html(Tags);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });

            }

        });

        var RelatedPosts = Backbone.View.extend({
            initialize: function (options) {
                this.el = $("#sidenavitem-related-content-box");
                this.target_el = $("#archive-side-nav #category-1");
            },
            render: function () {
                var _this = this;
                $.ajax({
                    type: "GET",
                    url: "includes/server_api.php/post as P, tags as T, tagsinpost as TP/TP.tid=T.id   and TP.pid=P.id  and P.id != " + PostID + " and T.tagname in (select distinct T.tagname from tags as T, tagsinpost as TP where TP.pid = " + PostID + " and TP.tid = T.id) group by P.heading order by count_of desc LIMIT 15/P.id, count(*) as count_of, P.heading, P.date, P.thumb ",
                    data: '',
                    cache: false,
                    success: function (html) {
                        var obj = JSON.parse(html);
                        var template = _.template(_this.el.html(), obj);
                        _this.target_el.append(template);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });

            }

        });

        var ArchivePage = Backbone.View.extend({
            initialize: function (options) {
                this.options = options;
                return this;
            },

            el: $('#indexcontent'),


            render: function (year, month, post, tag) {
                PostID = post;
                this.$el.html(archiveHTML);
                if (typeof post === 'undefined' || post === null) {
                    var archiveHeadPage = new ArchiveHeadPage();
                    archiveHeadPage.render(year, month, tag);
                } else {
                    var archivePostPage = new ArchivePostPage();
                    archivePostPage.render(year, month, post);
                }

                var tagCloud = new TagCloud();
                tagCloud.render();

                var sideArchiveNav = new SideArchiveNavigation();
                sideArchiveNav.render();









                ScrollTo('.main-content');

            },
            events: {
                //Listening for any events, usually click and binding a function with it.
                'click a[class=hideMeNav]': 'HideNav',
                'click .image': 'ShowImgFrame',
                'click #imgFrame': 'HideImgFrame',
                'submit form[id=commentForm]': 'sendComment',
                'click #viewOne':   'ShowViewOne',
                'click #viewTwo':   'ShowViewTwo',
                'click #viewThree':   'ShowViewThree'

            },
            HideNav: function (event) {
                if (HideNavTrigger) return;
                HideNavTrigger = true;
                event.preventDefault();
                var postid = $(event.currentTarget).parent().parent().attr("id");

                var delpost = '#list' + postid;


                if ($(delpost).is(":visible")) {
                    $(event.currentTarget).html('[+]');
                    $(delpost).slideUp('slow', function () {
                        $(this).hide();
                        HideNavTrigger = false;
                    });
                } else {
                    $(event.currentTarget).html('[-]');
                    $(delpost).slideDown('slow', function () {
                        $(this).show();
                        HideNavTrigger = false;
                    });
                }

                return false;
            },
            ShowImgFrame: function (event) {
                if (!ImageTrigger) {
                    $("#imgFrame img").attr("src", event.currentTarget.firstElementChild.src);
                    $("#imgFrame img").attr("alt", event.currentTarget.firstElementChild.alt);
                    $("#imgFrame img").attr("title", event.currentTarget.firstElementChild.alt);
                    $("#imgFrame").css('visibility', 'visible');
                    ImageTrigger = true;
                    return false;
                }
            },
            ShowViewOne: function (event) {
                event.currentTarget.style.background='#ccc';
                event.currentTarget.disabled=true;
                $("#viewTwo").prop('disabled', false);
                $("#viewThree").prop('disabled', false);
                $("#viewThree").css({'background':'var(--themeColor)'});
                $("#viewTwo").css({'background':'var(--themeColor)'});
                
                
                var struct2 = {};
                        JSON.parse(GlobalData).forEach(function (row) {
                            var date = new Date(row.date); // will extract date parts form Date object
                            var yyyy = date.getFullYear();
                            var mm = months[date.getMonth()]; // months come zero-indexed (from 0-11)
                            if (mm < 10) mm = '0' + mm; // left pad if needed
                            struct2[yyyy] = struct2[yyyy] || {}; // init if needed
                            struct2[yyyy][mm] = struct2[yyyy][mm] || []; // init if needed
                            //delete row.date; // drop date if desired
                            struct2[yyyy][mm].push(row); // add new record
                        });
                        var template = _.template($("#archivepage-grid-view1").html(), struct2);
                        $("#navpages").html(template);
            },
            ShowViewTwo: function (event) {
                event.currentTarget.style.background='#ccc';
                event.currentTarget.disabled=true;
                $("#viewOne").prop('disabled', false);
                $("#viewThree").prop('disabled', false);
                $("#viewThree").css({'background':'var(--themeColor)'});
                $("#viewOne").css({'background':'var(--themeColor)'});
                var template = _.template($("#archivepage-grid-view2").html(), JSON.parse(GlobalData));
                $("#navpages").html(template);
            },
            ShowViewThree: function (event) {
                event.currentTarget.style.background='#ccc';
                event.currentTarget.disabled=true;
                $("#viewTwo").prop('disabled', false);
                $("#viewOne").prop('disabled', false);
                $("#viewOne").css({'background':'var(--themeColor)'});
                $("#viewTwo").css({'background':'var(--themeColor)'});
                var template = _.template($("#archivepage-grid-view3").html(), JSON.parse(GlobalData));
                $("#navpages").html(template);
            },
            HideImgFrame: function (event) {
                if (ImageTrigger) {

                    $("#imgFrame").css('visibility', 'hidden');
                    ImageTrigger = false;
                }

            },
            sendComment: function (event) {
                event.preventDefault();
                if (CommentTrigger) return false;
                CommentTrigger = true;
                var commentVal = $('#commenttext').val();
                var dataString = JSON.stringify({
                    "uid": "0",
                    "pid": PostID,
                    "comment": commentVal
                }); {
                    $.ajax({
                        type: "POST",
                        url: "includes/server_api.php/comments",
                        data: dataString,
                        cache: false,
                        success: function (html) {
                            var template = _.template($("#comments-content-box").html(), [{
                                "comment": commentVal,
                                date: Date(),
                                new: '1'
                            }]);
                            $("#comments").prepend(template);
                            /*                                $("#comments").prepend('<div style="padding: 20px;" id="archivehead"> \
                                                                                        <div style="float: right" class="deletecomm"> </div> \
                                                                                        <p class="msg" style="margin-bottom: 5px;font-style: italic;">(#newcomment) Anonymous</p> \
                                                                                        <p style="margin-bottom: 5px;font-style: italic;"> '+comment+'</p> \
                                                                                        <h5 class="msg">Today</h5> \
                                                                                    </div>');*/
                            $('#commenttext').val('');
                            CommentTrigger = false;
                        },
                        error: function (e) {
                            console.log(e);
                        }
                    });
                    return false;
                }
            }








        });

        var ArchivePostPage = Backbone.View.extend({
            initialize: function (options) {
                this.options = options;
                _.bindAll(this, 'beforeRender', 'render', 'afterRender');
                var _this = this;
                this.render = _.wrap(this.render, function (render, a, b, c) {
                    _this.beforeRender(a);
                    render.call(_this, a, b, c);
                    _this.afterRender(a);
                    return _this;
                });
            },

            beforeRender: function (a) {
                if (!($.isNumeric(a)) || a === '0') {
                    $("#postcontent").html(fofHTML);
                    return false;
                }
                $("#navpage").hide();
                $("#postcontent").show();
            },

            render: function (year, month, post) {
                $('#post-wrapper').html(loaderHTML);
                $.ajax({
                    type: "GET",
                    url: "includes/server_api.php/post/id=" + post + " AND YEAR(Date) = " + year + " AND MONTH(Date) = " + month + "/heading,content,date,thumb",
                    data: '',
                    cache: false,
                    success: function (html) {

                        var obj = JSON.parse(html);
                        if (obj.length) {
                            var x = {
                                'heading': obj[0].heading,
                                'content': obj[0].content,
                                'thumb': obj[0].thumb
                            };
                            var template = _.template($("#post-content-box").html(), x);
                            $("#post-wrapper").html(template);

                            $("#archive-page-heading h1").html('<a href=#archive/' + year + '/' + month + '/' + post + '>' + obj[0].heading + '</a>');
                            $("#archive-side-nav #item1 .archive-side-nav-item-text").html(new Date(Date.parse(obj[0].date)).toDateString());

                            /*var data = '<h1 class="msg" id="postheading">'+ obj[0].heading +'</h1><h4 class="msg" id="postdate">'+ obj[0].date +'</h4><div id="postcontent"><div class="image" data-content="'+ obj[0].heading +'"><img style="margin: 0px auto;display: table;" src="'+ obj[0].thumb +'" alt="Hot maal" title=""></div>'+ obj[0].content +'</div>';
                            $("#postcontent").prepend(data);*/

                            highlightCode();
                        } else {
                            $("#postcontent").html(fofHTML);
                            $("#error-details").html("The post you are looking for does not exist.")
                        }
                    },
                    error: function (e) {
                        $('#indexcontent').html(fofHTML);
                        $("#error-details").html(e.status + ' (' + e.statusText + ') : Server not found.');
                    }
                });
            },

            afterRender: function (a) {

                var commentsOnPost = new CommentsOnPost();
                commentsOnPost.render();

                var tagsInPost = new TagsInPost();
                tagsInPost.render();

                var relatedPosts = new RelatedPosts();
                relatedPosts.render();





            }
        });

        var ArchiveHeadPage = Backbone.View.extend({

            initialize: function (options) {
                this.options = options;
                _.bindAll(this, 'beforeRender', 'render', 'afterRender');
                var _this = this;
                this.render = _.wrap(this.render, function (render, a, b, c) {
                    _this.beforeRender();
                    render.call(_this, a, b, c);
                    _this.afterRender();
                    return _this;
                });
            },

            beforeRender: function () {
                $("#postcontent").hide();
                $(".post-related-item").hide();
                $("#navpage").show();
            },

            render: function (year, month, tag) {
                var result = {};
                displayMessage = '';
                composedURL = '';
                if (typeof tag === 'undefined' || tag === null) {
                    displayMessage = "All posts";
                    composedURL = "includes/server_api.php/post/";
                    if (typeof year !== 'undefined' && year !== null) {
                        composedURL += "YEAR(Date) = " + year;
                        displayMessage = "All posts from " + year;
                        if (typeof month !== 'undefined' && month !== null) {
                            composedURL += " AND MONTH(Date) = " + month;
                            displayMessage = "All posts from " + months[month - 1] + ', ' + year;
                        }
                    } else {
                        composedURL += '0';
                    }
                    composedURL += "/id,heading,date,thumb,LEFT(content, 100) as preview";
                } else {
                    displayMessage = "All posts tagged #" + tag;

                    composedURL = "includes/server_api.php/post as P, tags as T, tagsinpost as TP/TP.tid=T.id and TP.pid=P.id and T.tagname = '" + tag + "'/distinct P.id, P.heading, P.date, P.thumb, LEFT(P.content, 100) as preview";
                }

                $('#navpages').html(loaderHTML);

                $.ajax({
                    type: "GET",
                    url: composedURL,
                    data: '',
                    cache: false,
                    success: function (html) {
                        GlobalData = html;
                        if ($.isEmptyObject(JSON.parse(html))) {
                            $('#indexcontent').html(fofHTML);
                            $("#error-details").html('No posts found.');
                        }
                        $('#navpages').html('');
                        
                        
                        var struct2 = {};
                        JSON.parse(html).forEach(function (row) {
                            var date = new Date(row.date); // will extract date parts form Date object
                            var yyyy = date.getFullYear();
                            var mm = months[date.getMonth()]; // months come zero-indexed (from 0-11)
                            if (mm < 10) mm = '0' + mm; // left pad if needed
                            struct2[yyyy] = struct2[yyyy] || {}; // init if needed
                            struct2[yyyy][mm] = struct2[yyyy][mm] || []; // init if needed
                            //delete row.date; // drop date if desired
                            struct2[yyyy][mm].push(row); // add new record
                        });
                        var template = _.template($("#archivepage-grid-view1").html(), struct2);
                        $("#navpages").html(template);
                        
                        /*var template = _.template($("#archivepage-grid-view2").html(), JSON.parse(html));
                        $("#navpages").html(template);*/
                        

                        /*for (var id in struct2) {
                            result = '';
                            result += '<li class="hideme coolclass" id="' + id + '"><div style=" display: flex;font-weight: bold;color: white;line-height:1em;"><a class="hideMeNav">[-]</a><h3 class="msg" style="background: var(--themeColor);padding: 10px;flex: 1;color: inherit;margin-left: 10px;">' + id + '</h3></div><ul id="list' + id + '">';


                            for (var _id in struct2[id]) {

                                result_divs = '';
                                result += "<li class='hideme coolclass' id='" + id + _id + "'><div style='display: flex;font-weight: bold;color: white;line-height:1em;'><a class='hideMeNav'>[-]</a><h3 class='msg' style='background: var(--themeColor);padding: 10px;flex: 1;color: inherit;margin-left: 10px;'>" + _id + "</h3></div> <ul style='display: flex;flex-wrap: wrap;' id='list" + id + _id + "'>";



                                for (var __id in struct2[id][_id]) {
                                    thumbnail = (struct2[id][_id][__id].thumb) ? "<div class='archiveiconsimg'><img src='" + struct2[id][_id][__id].thumb + "' ></div>" : '';
                                    result_divs = "<li class='coolclass anchor openpost' id='" + struct2[id][_id][__id].id + "'><a href='#archive/" + id + '/' + (months.indexOf(_id) + 1) + '/' + struct2[id][_id][__id].id + "' style='display:block'>" + thumbnail + struct2[id][_id][__id].heading + result_divs;
                                }
                                result += result_divs + '</ul></li>';
                            }
                            result += '</ul></li>';
                            $('#navpages').prepend(result);
                        }*/
                            
                        
                        $('#archive-page-heading h1').html(displayMessage);
                    },
                    error: function (e) {
                        $('#indexcontent').html(fofHTML);
                        $("#error-details").html(e.status + ' (' + e.statusText + ') : Server not found.');
                    }
                });
            },

            afterRender: function () {

            }
        });

        return ArchivePage;
    });