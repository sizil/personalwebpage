define([
  'jquery',
  'underscore',
  'backbone',
  'handlebars',
  'jcookie',
  '../app',
  'text!templates/contact.html'
], function ($, _, Backbone, Handlebars, jcookie, app, ContactHTML) {
    var ContactPage = Backbone.View.extend({
        el: $('#indexcontent'),
        render: function () {
            var data = {};
            var compiledTemplate = _.template(ContactHTML, data);
            this.$el.html(compiledTemplate);
            ScrollTo('.main-content');
        },
        events: {
            'submit form[id=contactForm]' : 'sendMsg'
        },
        sendMsg: function(event){
            //Same thing as ABOVE just using different interaction
            //POST
            //GET
            //PUT
            //DELETE
            if(!($('#username').val()&&$('#useremail').val()&&$('#usermsg').val())) {
                $("#sendMsg").val("All fields are required!");
                $("#sendMsg").css({background:'red'});
                $(this).delay(2000).queue(function() {
                    $("#sendMsg").val("Submit");
                    $("#sendMsg").css({background:'var(--themeColor)'});
                    $(this).dequeue();
                });
                $("#sendMsg").html("All fields are required!");
                return false;
            }
            var js_obj = {"username":$('#username').val(),"useremail":$('#useremail').val(),"message":$('#usermsg').val()};
            
            var dataString = JSON.stringify( js_obj ); {
                $.ajax({
                    type: "POST",
                    url: "includes/server_api.php/contact",
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        if(html) $('#contactForm').html('Message Sent');
                        $("#msgsentinfo").html('');
                    },
                    error: function (e) {
                        $("#msgsentinfo").html(e.status + ' (' + e.statusText + ') : Server not found. Sorry.');
                    }
                });
                return false;
            }
        }
    });
    return ContactPage;
});