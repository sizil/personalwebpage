define([
  'jquery',
  'underscore',
  'backbone',
  'handlebars',
  'jcookie',
  '../app',
  'text!templates/work.html',
  'text!templates/404.html',
  'text!templates/loader.html',
    
], function ($, _, Backbone, Handlebars, jcookie, app, workHTML, fofHTML, loaderHTML) {
    var WorkPage = Backbone.View.extend({
        el: $('#indexcontent'),
        render: function () {
            var _this = this;
            _this.$el.html(loaderHTML);
                $.ajax({
                    type: "GET",
                    url: "includes/server_api.php/work",
                    data: '',
                    cache: false,
                    success: function (html) {
                        var work = JSON.parse(html);
                        var compiledTemplate = _.template(workHTML, work);
                        _this.$el.html(compiledTemplate);
                        
                        
                        ScrollTo('.main-content');
                       },
                    error: function (e) {
                        $('#indexcontent').html(fofHTML);
                        $("#error-details").html(e.status + ' (' + e.statusText + ') : Server not found.');
                    }
                });
                return false;
            
            
            
        }
    });
    return WorkPage;
});