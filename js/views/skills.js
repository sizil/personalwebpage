define([
  'jquery',
  'underscore',
  'backbone',
  'handlebars',
  'jcookie',
  '../app',
  'text!templates/skills.html'
], function ($, _, Backbone, Handlebars, jcookie, app, skillsHTML) {
    var SkillsPage = Backbone.View.extend({
        el: $('#indexcontent'),
        render: function () {
            var data = {};
            var compiledTemplate = _.template(skillsHTML, data);
            this.$el.html(compiledTemplate);
            ScrollTo('.main-content');
        }
    });
    return SkillsPage;
});