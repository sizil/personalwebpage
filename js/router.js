siteTitle = 'Sizil Krishna';

define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'jcookie',
    'views/home',
    'views/work',
    'views/skills',
    'views/contact',
    'views/archive',
    'views/tags',
    'views/404'
], function ($, _, Backbone, Handlebars, jcookie, HomePage,  WorkPage, SkillsPage, ContactPage, ArchivePage, TagsPage, FoFPage) {
    var AppRouter = Backbone.Router.extend({
        routes: {
            ':*':'home',
            'home' : 'home',
            'home/:a' : 'home',
            'work':'work',
            'skills':'skills',
            'contact':'contact',
            'archive':'archive',
            'archive/:year':'archive',
            'archive/:year/:month':'archive',
            'archive/:year/:month/:post':'archive',
            'tag/:tag':'tags',
            '*whatever' : '404'
            
        },
        home: function (a) {
            document.title = siteTitle + ' - Home';
            homePage = new HomePage({route: a});
            homePage.render();
        },
        work:function(){
            document.title = siteTitle + ' - Work';
            workPage = new WorkPage;
            workPage.render();
        },
        skills:function(){
            document.title = siteTitle + ' - Skills';
            skillsPage = new SkillsPage;
            skillsPage.render();
            
        },
        contact:function(){
            document.title = siteTitle + ' - Contact Me';
            contactPage = new ContactPage;
            contactPage.render();
        },
        archive:function(year, month, post){
            document.title = siteTitle + ' - Archives';
            archivePage= new ArchivePage();
            archivePage.render(year, month, post, null);
        },
        tags:function(tag){
            document.title = siteTitle + ' - Tags';
            tagsPage = new TagsPage;
            tagsPage.render(tag);
        },
        404: function(){
            document.title = siteTitle + ' - 404';
            fofPage = new FoFPage;
            fofPage.render();
        }
        
    });

    var initialize = function () {
        var app_router = new AppRouter;
        Backbone.history.start();
        
    };
    return {
        initialize: initialize,
        AppRouter: AppRouter
    }
});