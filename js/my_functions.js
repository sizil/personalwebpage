var HideNavTrigger = false;
var ImageTrigger = false;
var CommentTrigger = false;
var GlobalData = {};
var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


var highlightCode = function () {
    this.$('pre code').each(function (i, block) {
        hljs.highlightBlock(block);
    });    
}

var ScrollTo = function(element){
    $('html,body').animate({
        scrollTop: $(element).offset().top
    },'slow');
}

var changeColor = function(){
    document.documentElement.style.setProperty('--themeColor', '#' + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6));
}