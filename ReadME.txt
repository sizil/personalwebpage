
            //POST
            //GET
            //PUT
            //DELETE
            
Database:   SIZIL
Tables:     SIZIL
                - USERS
                - COMMENTS
                
            
            
POST:   //Inserts the data into table.
        URL:                    server_api.php/USERS
        DATASTRING:             {"username":"mercurial",useremail:"sizil.krishna23@gmail.com",name:"Sizil Krishna"} //JSON FORMAT
        
DELETE: //Deletes a row from table.
        URL:                    server_api.php/COMMENTS/4                                          //delete from `COMMENTS` where id="4"
        
PUT:    //Updates the Database.
        URL:                    server_api.php/COMMENTS/4       //update `COMMENTS` set `username`='mercurial', `message`='Sizil Krishna' where id="4"
        DATASTRING:             {"username":"mercurial",message:"Sizil Krishna"} //JSON FORMAT
        
GETL    //Gets the data from Database
        URL:                    server_api.php/COMMENTS         //select * from COMMENTS
                                server_api.php/COMMENTS/0       //select * from COMMENTS
                                server_api.php/COMMENTS/id=4       //select * from COMMENTS where id="4"
                                server_api.php/COMMENTS/0/commid,comment       //select commid,comment from COMMENTS
                                server_api.php/COMMENTS/id=4/commid,comment       //select commid,comment from COMMENTS where id="4"
                                server_api.php/COMMENTS/id=4 and pid=7/commid,comment       //select commid,comment from COMMENTS where id="4" and pid="7"
                                server_api.php/COMMENTS as C NATURAL JOIN USERS as U/C.id=4 and U.pid=7/C.commid,U.comment       
                                            //select C.commid,U.comment from COMMENTS where C.id="4" and U.pid="7"
                                
                                {                           
                                server_api.php/tags as T natural join tagsinpost as TP/T.id=TP.tid and TP.pid=17/T.tagname, T.id  
                                =
                                select T.tagname, T.id from tags as T natural join tagsinpost as TP where T.id=TP.tid and TP.pid=17
                                }

                                