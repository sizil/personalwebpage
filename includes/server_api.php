<?php



    $method = $_SERVER['REQUEST_METHOD'];
    $request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	
    $input = is_null(json_decode(file_get_contents('php://input'), true)) ? [] : json_decode(file_get_contents('php://input'), true);
    $link = mysqli_connect('localhost', 'root', '', 'upgrade_sizil');
    mysqli_set_charset($link, 'utf8');

//  $table = preg_replace('/[^a-z0-9_]+/i','',array_shift($request));
    $table = array_shift($request);
    $key = array_shift($request);
    $table_columns = array_shift($request);

	
    $columns = preg_replace('/[^a-z0-9_]+/i','',array_keys($input));
    $values = array_map(function ($value) use ($link){
        if($value == null) return null;
        return mysqli_real_escape_string($link, (string)$value);
    }, array_values($input));

    $set = '';
    for($i=0 ; $i < count($columns) ; $i++){
        $set.=($i>0?',':'').'`'.$columns[$i].'`=';
        $set.=($values[$i]===null?'NULL':'"'.$values[$i].'"');
    }

    switch($method){
        case 'GET':
            $sql = "select ".($table_columns?$table_columns:'*')." from $table".($key?" WHERE $key":''); break;
        case 'PUT':
            $sql = "update `$table` set $set where id=$key"; break;
        case 'POST':
            $sql = "insert into `$table` set $set"; break;
        case 'DELETE':
            $sql = "delete from `$table` where id=$key"; break;
    }

    $result = mysqli_query($link, $sql);

    if(!$result){
        http_response_code(404);
        die(mysqli_error());
    }

    if($method == 'GET'){
        echo '[';
        for($i=0;$i<mysqli_num_rows($result);$i++){
            echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
        }
        echo ']';
    } elseif ($method == 'POST'){
        echo mysqli_insert_id($link);
    } else{
        echo mysqli_affected_rows($link);
    }
    mysqli_close($link);
?>

